from django import forms
from .models import *


class SeekerDetailForm(forms.ModelForm):
    class Meta:
        model = Jobseeker
        fields = ['name', 'dob','province','district', 'address', 'phone_no', 'mobile_no', 'resume', 'photo']
