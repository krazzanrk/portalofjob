from django.utils.text import slugify
from sorl.thumbnail import ImageField

from custom_auth.models import User
from django.db import models
from home.models import *

# Create your models here.
from home.validators import *


class Jobseeker(models.Model):
    name = models.CharField(max_length=100, validators=[name_validation])
    dob = models.DateField(validators=[Date_validation])
    gender = models.ForeignKey(Gender, on_delete=models.CASCADE)
    province = models.ForeignKey(Provience, related_name='jobseeker_provience', on_delete=models.CASCADE)
    district = models.ForeignKey(District, related_name='jobseeker_district', on_delete=models.CASCADE)
    address = models.CharField(max_length=50)
    phone_no = models.CharField(blank=True, max_length=9, validators=[phone_validation])
    mobile_no = models.CharField(blank=True, null=True, validators=[mobile_validation], max_length=10)
    education = models.ForeignKey(Education, on_delete=models.CASCADE)
    resume = models.FileField(upload_to='media/')
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    photo = ImageField()
    slug = models.SlugField()

    def __str__(self):
        return self.slug

    def save(self, *args, **kwargs):
        value = self.name
        self.slug = slugify(value, allow_unicode=True)
        super().save(*args, **kwargs)
