from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import PasswordChangeView, PasswordResetView
from django.core.files.storage import FileSystemStorage
from django.shortcuts import render, redirect
from django.urls import reverse_lazy

from .forms import *
from company.models import *

# Create your views here.
from django.views.generic import *
from .models import *

class JobSeekerCategoryView(TemplateView):
    template_name = 'job_category.html'

class JobSeekerDetailView(CreateView):
    template_name = 'seeker_detail.html'
    model = Jobseeker
    form_class = SeekerDetailForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['genders'] = Gender.objects.all()
        context['educations'] = Education.objects.all()


        try:
            context['seeker'] = Jobseeker.objects.get(user_id=self.request.user)
        except Exception as e:
            print(e)

        return context

    def post(self, request, *args, **kwargs):
        # form = self.get_form()
        form = SeekerDetailForm(request.POST, request.FILES)
        print("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++")

        if form.is_valid():

            ge = request.POST.get('gen')
            ed = request.POST.get('edu')

            gender = Gender.objects.get(gender=ge)
            education = Education.objects.get(education=ed)

            sucess = form.save(commit=False)
            sucess.user = request.user
            sucess.education_id = education.id
            sucess.gender_id = gender.id


            sucess.save()

            return redirect('home:joblist')

        else:
            print(form.errors)

        return redirect('jobseeker:sdhome')


class SeekerIndexView(TemplateView):
    template_name = 'seeker dashboard/seeker_index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        try:
            context['seeker'] = Jobseeker.objects.get(user_id=self.request.user)
        except Exception as e:
            print(e)
        return context


class JobsekkerTableView(ListView):
    model = AppliedBySeeker
    template_name = 'seeker dashboard/applied job-table.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        user = self.request.user

        context['applybyuser'] = AppliedBySeeker.objects.filter(applicant_name__user_id=user)
        try:
            context['seeker'] = Jobseeker.objects.get(user_id=user)
        except Exception as e:
            print(e)
        return context


class SeekerDashboardView(TemplateView):
    model = Jobseeker
    # template_name = 'jobseeker dashboard/user_dashboard.html'
    template_name = 'seeker dashboard/dashboard2.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        try:

            context['seeker'] = Jobseeker.objects.get(user_id=self.request.user)
        except Exception as e:
            print(e)
        return context


class UserProfileUpdateView(UpdateView):
    model = Jobseeker
    template_name = 'seeker dashboard/userprofile_update_form.html'
    fields = ['name', 'dob', 'gender', 'province', 'district', 'address', 'phone_no', 'mobile_no', 'education',
              'resume', 'photo']
    success_url = reverse_lazy('jobseeker:sindex')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        try:

            context['seeker'] = Jobseeker.objects.get(user_id=self.request.user)
        except Exception as e:
            print(e)
        return context


class ChangePassword(PasswordChangeView):
    template_name = 'seeker dashboard/jobseeker_password_change.html'
    success_url = reverse_lazy('jobseeker:sdhome')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        try:

            context['seeker'] = Jobseeker.objects.get(user_id=self.request.user)
        except Exception as e:
            print(e)
        return context


def load_province(request):
    pro_id = request.GET.get('province')
    districts = District.objects.filter(provience_id=pro_id)
    return render(request, 'seeker dashboard/drop_down_dis_pro.html', {'districts': districts})
