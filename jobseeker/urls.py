from django.urls import path

from jobseeker import views
from jobseeker.views import *

app_name = "jobseeker"

urlpatterns = [

    path('category/', JobSeekerCategoryView.as_view(), name='jobcategory'),

    path('seekerdetail/', JobSeekerDetailView.as_view(), name='seekerdetail'),
    path('seekertable/', JobsekkerTableView.as_view(), name='table'),
    path('seekerdashboard/', SeekerDashboardView.as_view(), name='sdhome'),
    path('seekerdashboard/base', SeekerIndexView.as_view(), name='sindex'),
    path('seekerdetail/<slug:slug>/update', UserProfileUpdateView.as_view(), name='supdate'),
    path('change_password/', ChangePassword.as_view(), name='password-change'),
    path('ajax/load-dis/', views.load_province, name='job_ajax_load_district'),

]
