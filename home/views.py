import datetime
from django.utils import timezone

from django.db.models import Q, Count
from django.shortcuts import render, redirect

# Create your views here.
from django.views.generic import ListView, TemplateView, DetailView
from company.models import *


class IndexView(ListView):
    template_name = 'index.html'
    model = JobPost
    paginate_by = 3

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)

        context['job_lists'] = JobPost.objects.all()
        context['top_rated'] = JobPost.objects.order_by('?')[:4]
        context['cat'] = Category.objects.all()
        # context['job_by_location'] = JobPost.objects.order_by('?')[:10]
        context['top_rated'] = JobPost.objects.all().order_by('?')[:4]
        a = District.objects.annotate(Count('companydetail_district__jobpost_company')).order_by(
            '-companydetail_district__jobpost_company__count')[:3]
        job_location = []
        for i in a:
            z = JobPost.objects.filter(company__district__district=i).count()
            aa = {'district': i.district, 'count': z}
            job_location.append(aa)
        context['jbl'] = job_location

        return context

    def post(self, request, *args, **kwargs):

        x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')

        if x_forwarded_for:
            ipaddress = x_forwarded_for.split(',')[-1].strip()
            print('---------------', ipaddress)
        else:
            ipaddress = request.META.get('REMOTE_ADDR')
            print('-----------s----', ipaddress)
        get_ip = IPTracker()  # imported class from model
        b = request.POST.get("jobtitle")
        get_ip.job_ip_id = b
        a = JobPost.objects.get(id=b)
        j_id = IPTracker.objects.filter(job_ip_id=b)
        print('-------------', j_id)
        temp = 0

        try:

            if j_id.exists():
                for i in j_id:

                    if i.ip_data == ipaddress:
                        temp = 0
                        break

                    else:
                        temp = 1

                if temp == 1:
                    get_ip.job_ip_id = request.POST.get("jobtitle")
                    get_ip.user_ip_id = request.POST.get("userid")
                    get_ip.ip_data = ipaddress

                    get_ip.save()


            else:
                get_ip.job_ip_id = request.POST.get("jobtitle")
                get_ip.user_ip_id = request.POST.get("userid")
                get_ip.ip_data = ipaddress

                get_ip.save()

        except Exception as e:
            print(e)

        return redirect("jobdetail/" + a.slug)


class SearchView(ListView):
    template_name = 'search_result.html'
    model = JobPost

    def get(self, request, *args, **kwargs):
        title = request.GET.get('title')
        location = request.GET.get('location')
        category = request.GET.get('category')

        a = District.objects.annotate(Count('companydetail_district__jobpost_company')).order_by(
            '-companydetail_district__jobpost_company__count')[:3]
        job_location = []
        for i in a:
            z = JobPost.objects.filter(company__district__district=i).count()
            aa = {'district': i.district, 'count': z}
            job_location.append(aa)


        search_set = JobPost.objects.filter()

        if request.GET.get('title') and request.GET.get('location') and request.GET.get('category'):
            search_set = search_set.filter(job_title__icontains=title, company__district__district__icontains=location,
                                           category__category__icontains=category)

        elif request.GET.get('title') and request.GET.get('location') or request.GET.get('category'):
            search_set = search_set.filter(job_title__icontains=title, company__district__district__icontains=location,
                                           category__category__icontains=category)
        elif request.GET.get('title') or request.GET.get('location') and request.GET.get('category'):
            search_set = search_set.filter(job_title__icontains=title, company__district__district__icontains=location,
                                           category__category__icontains=category)
        elif request.GET.get('title') or request.GET.get('location') or request.GET.get('category'):
            search_set = search_set.filter(job_title__icontains=title, company__district__district__icontains=location,
                                           category__category__icontains=category)

        context = {

            'filtered_job': search_set,
            'top_rated': JobPost.objects.all().order_by('-id')[:6],
            'jbl':job_location

        }

        return render(request, 'search_result.html', context)


class FaqView(TemplateView):
    template_name = 'FAQ.html'
    model = Faq

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['jobseekerfaq'] = Faq.objects.filter(role='jobseeker')
        context['companyfaq'] = Faq.objects.filter(role='company')
        return context


class JobListView(ListView):
    template_name = 'job_listt.html'
    model = JobPost
    paginate_by = 1

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        # context['joblists'] = JobPost.objects.all()
        context['top_jobs'] = JobPost.objects.all().order_by('?')[:6]
        context['job_by_location'] = JobPost.objects.order_by('?')[:10]
        context['top_rated'] = JobPost.objects.order_by('?')[:4]
        a = District.objects.annotate(Count('companydetail_district__jobpost_company')).order_by(
            '-companydetail_district__jobpost_company__count')[:3]
        job_location = []
        for i in a:
            z = JobPost.objects.filter(company__district__district=i).count()
            aa = {'district': i.district, 'count': z}
            job_location.append(aa)
        context['jbl'] = job_location
        return context


class JobDetailView(DetailView):
    template_name = 'job_detail.html'
    model = JobPost

    def get_context_data(self, **kwargs):
        slug = self.kwargs['slug']
        job = JobPost.objects.get(slug=slug)
        context = super().get_context_data(**kwargs)
        context['jdetail'] = JobPost.objects.get(slug=slug)
        context['job_by_location'] = JobPost.objects.order_by('?')[:10]
        a = District.objects.annotate(Count('companydetail_district__jobpost_company')).order_by(
            '-companydetail_district__jobpost_company__count')[:3]
        job_location = []
        for i in a:
            z = JobPost.objects.filter(company__district__district=i).count()
            aa = {'district': i.district, 'count': z}
            job_location.append(aa)
        context['jbl'] = job_location

        if self.request.user == True:
            context['top_rated'] = JobPost.objects.filter(company__user_id=self.request.user)
        else:
            context['top_rated'] = JobPost.objects.order_by('?')[:4]
        context['count'] = IPTracker.objects.filter(job_ip__slug=slug).count()
        context['applied'] = AppliedBySeeker.objects.filter(job_title__slug=slug).count()

        try:

            context['alreadyapply'] = AppliedBySeeker.objects.filter(job_title__slug=slug,
                                                                     applicant_name__user_id=self.request.user).exists()

        except Exception as e:
            print(e)

        a = job.deadline - date.today()

        week = int(a.days / 7)
        day = a.days % 7
        print(a.days, week, day)
        context['afterdeadline'] = a.days

        context['week'] = week
        context['days'] = day
        context['company'] = CompanyDetail.objects.get(jobpost_company=job)

        try:
            context['seeker'] = Jobseeker.objects.get(user_id=self.request.user)
        except Exception as e:
            print(e)
        return context


class ByLocationView(ListView):

    template_name = 'by_location.html'
    model = JobPost
    paginate_by = 2
    context_object_name = 'BOM'
    queryset = JobPost.objects.filter(company__district__district__iexact=District.district)


    def get_context_data(self, *, object_list=None, **kwargs):
        context=super().get_context_data(**kwargs)
        district=self.kwargs.get('slug',None)
        queryset=JobPost.objects.filter(company__district__district__iexact=district)
        context['jl']=queryset

        a = District.objects.annotate(Count('companydetail_district__jobpost_company')).order_by(
            '-companydetail_district__jobpost_company__count')[:3]
        job_location = []
        for i in a:
            z = JobPost.objects.filter(company__district__district=i).count()
            aa = {'district': i.district, 'count': z}
            job_location.append(aa)
        context['jbl'] = job_location


        a=JobPost.objects.all()
        b=IPTracker.objects.all()
        count=0
        storing_count_inlist=[]


        for i in a:
            for j in b:
                 if i.id ==j.job_ip_id:
                     count=count + 1
            print('job title:',i.job_title,'\n','views count:',count)

            storing_count_inlist.append({'job':i.job_title,'count':count})
            print(storing_count_inlist)
            count = 0
            try:
                context['top_rated'] = JobPost.objects.order_by('?')[:2]

            except Exception as e:
                print(e)




        return context

