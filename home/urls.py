from django.urls import path
from home.views import *

app_name = "home"

urlpatterns = [

    path('', IndexView.as_view(), name='home'),
    path('search/', SearchView.as_view(), name='search'),
    path('faq/', FaqView.as_view(), name='faq'),
    path('jobdetail/<slug:slug>/', JobDetailView.as_view(), name='jobdetail'),
    path('list/', JobListView.as_view(), name='joblist'),
    path('location/<slug:slug>',ByLocationView.as_view(),name='by_location')


]
