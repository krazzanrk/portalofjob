from datetime import date

from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _


def name_validation(value, *args, **kwargs):
    if value.isalpha() == False and value == ' ':
        raise ValidationError(
            _('%(value)s is not a string'),
            params={'value': value},
        )


def phone_validation(value, *args, **kwargs):
    if value.isnumeric() == False or (len(value) < 9):
        raise ValidationError(
            _('warning %(value)s is not a valid phone number!!!!'),
            params={'value': value},
        )


def mobile_validation(value, *args, **kwargs):
    if value.isnumeric() == False or (len(value) < 10) or (value[0] != '9'):
        print(type(value))
        raise ValidationError(
            _('warning %(value)s is not a valid phone number!!!!'),
            params={'value': value},
        )


def Date_validation(value, *args, **kwargs):
    today_date = date.today()
    entered_date = 365 * (today_date.year - value.year) + 30 * (today_date.month - value.month) + (
            today_date.day - value.day)

    valid_age = 16 * 365

    if entered_date < valid_age or value > today_date:
        raise ValidationError(
            _(' is not valid age'),
            params={'value': value}
        )


def registration_date_validation(value, *args, **kwargs):
    today_date = date.today()

    if value > today_date:
        raise ValidationError(
            _('%(value) is not valid date!!!!'),
            params={'value': value}
        )


def deadline_registration(value, *args, **kwargs):
    today_date = date.today()

    deadline_month = 15 * (value.month - today_date.month) + (value.day - today_date.day)

    if deadline_month > 15 or value < today_date:
        raise ValidationError(
            _('deadline must be maximum 15 days')
        )
