from django.contrib import admin
from home.models import *

# Register your models here.
admin.site.register(Education)
admin.site.register(Gender)
admin.site.register(Category)
admin.site.register(Provience)
admin.site.register(District)
admin.site.register(JobType)
admin.site.register(Level)
admin.site.register(Faq)
