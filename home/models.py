from django.db import models


# Create your models here.

class Education(models.Model):
    education = models.CharField(max_length=150)

    def __str__(self):
        return self.education


class Gender(models.Model):
    gender = models.CharField(max_length=15)

    def __str__(self):
        return self.gender


class Category(models.Model):
    category = models.CharField(max_length=50)

    def __str__(self):
        return self.category


class Provience(models.Model):
    provience_name = models.CharField(max_length=50)

    def __str__(self):
        return self.provience_name


class District(models.Model):
    district = models.CharField(max_length=25)
    provience = models.ForeignKey(Provience, related_name='district_provience', on_delete=models.CASCADE)

    def __str__(self):
        return self.district


class JobType(models.Model):
    job_type = models.CharField(max_length=15)

    def __str__(self):
        return self.job_type


class Level(models.Model):
    job_level = models.CharField(max_length=25)

    def __str__(self):
        return self.job_level


RoleChoices = (
    ('company', 'Company'),
    ('jobseeker', 'JobSeeker')
)


class Faq(models.Model):
    question = models.CharField(max_length=100)
    answer = models.TextField()
    role = models.CharField(choices=RoleChoices, max_length=50)

    def __str__(self):
        return str(self.id)

