from django.urls import path

from blog.views import *

app_name = 'blog'

urlpatterns = [

    path('', BlogView.as_view(), name='blog'),
    path('single/<slug:slug>', SingleBlogView.as_view(), name='single_blog'),

    path('single/<slug:slug>/update', SingleBlogView.as_view(), name='single_update'),
    path('comment/', CommentView.as_view()),
]
