from django.utils.text import slugify

from custom_auth.models import User
from django.db import models
from home.models import *
from home.validators import *
from jobseeker.models import *
import random


# Create your models here.

class CompanyDetail(models.Model):
    company_name = models.CharField(max_length=25)
    province = models.ForeignKey(Provience, on_delete=models.CASCADE, related_name='companydetail_province')
    district = models.ForeignKey(District, on_delete=models.CASCADE, related_name='companydetail_district')
    company_address = models.CharField(max_length=50)
    company_type = models.CharField(max_length=50)
    contact_no = models.CharField(blank=True, max_length=9, validators=[phone_validation])
    mobile_no = models.CharField(blank=True, null=True, validators=[mobile_validation], max_length=10)
    company_image = ImageField()
    slug = models.SlugField()
    company_registration = models.DateField(null=True, blank=True, validators=[registration_date_validation])
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.company_name

    def save(self, *args, **kwargs):
        value = self.company_name
        self.slug = slugify(value, allow_unicode=True)
        super().save(*args, **kwargs)


class JobPost(models.Model):
    job_title = models.CharField(max_length=50)
    company = models.ForeignKey(CompanyDetail, on_delete=models.CASCADE, related_name='jobpost_company')
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    level_of_job = models.ForeignKey(Level, on_delete=models.CASCADE, related_name='jobpost_joblevel')
    vacancy = models.IntegerField(null=True)
    experience = models.CharField(max_length=25)
    education = models.ForeignKey(Education, models.CASCADE)
    salary = models.IntegerField(blank=True)
    negotiable = models.BooleanField(blank=True)
    job_type = models.ForeignKey(JobType, on_delete=models.CASCADE, related_name='jobpost_jobtype')
    description = models.TextField()
    requirement = models.TextField()
    pub_date = models.DateField(auto_now=True)
    deadline = models.DateField(validators=[deadline_registration])
    slug = models.SlugField()

    def __str__(self):
        return self.slug

    def save(self, *args, **kwargs):
        number = random.randint(0, 100000)

        value = self.company.company_name + self.job_title + str(number)
        self.slug = slugify(value, allow_unicode=True)
        super().save(*args, **kwargs)


class AppliedBySeeker(models.Model):
    job_title = models.ForeignKey(JobPost, on_delete=models.CASCADE)
    applicant_name = models.ForeignKey(Jobseeker, on_delete=models.CASCADE)
    applied_date = models.DateField(auto_now=True)
    status = models.BooleanField(blank=True)
    accepted = models.BooleanField(blank=True)

    def __str__(self):
        return self.applicant_name


class IPTracker(models.Model):
    user_ip = models.ForeignKey(User, null=True, blank=True, related_name="iptracker_user_ip", on_delete=models.CASCADE)
    job_ip = models.ForeignKey(JobPost, related_name="iptracker_job_ip", on_delete=models.CASCADE)
    ip_data = models.CharField(max_length=50)
    date_time=models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.ip_data
