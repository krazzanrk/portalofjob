from django.urls import path

from company import views
from company.views import *

app_name = 'company'

urlpatterns = [
    path('jobpost/', JobPostView.as_view(), name='jobpost'),
    path('jobpost/c_list', CompanyJobListView.as_view(), name='cjoblist'),
    path('jobpost/chome', CompanyHomeView.as_view(), name='jobhome'),
    path('jobpost/base', CompanyBaseView.as_view(), name='cbase'),
    path('profile/', CompanyProfileView.as_view(), name='cprofile'),

    path('companydetail/', CompanyDetailView.as_view(), name='companydetail'),
    path('companydetail/<slug:slug>/update', CompanyUpdateView.as_view(), name='cupdate'),
    path('apply/', AppliedBySeekerView.as_view(), name='apply'),
    path('jobpost/<slug:slug>/update', JobUpdateView.as_view(), name='jobupdate'),
    path('jobpost/<slug:slug>/delete', JobDeleteView.as_view(), name='jobdelete'),
    path('change_password/', ChangePassword.as_view(), name='password_change'),

    path('ajax/load-cities/', views.load_dis, name='ajax_load_district'),
    path('notify/', CompanyNotificationView.as_view(), name='companynotify'),

    # path('faq/create', FAQCreateView.as_view())
]
