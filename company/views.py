from urllib import request

from django import forms
from django.contrib.auth.views import PasswordChangeView
from django.shortcuts import render, redirect
from django.urls import reverse_lazy

from company.models import *

# Create your views here.
from django.views.generic import *


class JobPostView(CreateView):
    template_name = 'new-post.html'
    model = JobPost
    fields = ['job_title', 'vacancy', 'experience', 'salary', 'negotiable', 'description', 'requirement', 'deadline']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['categories'] = Category.objects.all()
        context['loc'] = CompanyDetail.objects.all()
        context['edu'] = Education.objects.all()
        context['jobs'] = JobPost.objects.all()
        context['jobtypes'] = JobType.objects.all()
        context['levels'] = Level.objects.all()

        context['com'] = CompanyDetail.objects.get(user_id=self.request.user)
        try:

            context['company_detail'] = CompanyDetail.objects.get(user_id=self.request.user)

        except Exception as e:
            print(e)
        return context

    def post(self, request, *args, **kwargs):
        form = self.get_form()

        if form.is_valid():
            cate = request.POST.get('cate')
            edu = request.POST.get('ed')
            j_level = request.POST.get('level')
            j_type = request.POST.get('jtype')
            category = Category.objects.get(category=cate)
            edu = Education.objects.get(education=edu)
            joblevel = Level.objects.get(job_level=j_level)
            jobtype = JobType.objects.get(job_type=j_type)
            sucess = form.save(commit=False)
            sucess.category_id = category.id
            sucess.education_id = edu.id
            sucess.job_type_id = jobtype.id
            sucess.level_of_job_id = joblevel.id

            user = request.user
            company = CompanyDetail.objects.all()
            for i in company:
                if user.id == i.user_id:
                    sucess.company_id = i.id

            sucess.save()

            return redirect('company:jobhome')

        else:
            print(form.errors)

        return redirect('home:home')


class CompanyBaseView(TemplateView):
    model = CompanyDetail
    template_name = '__company_base.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        user = self.request.user
        try:
            context['com'] = CompanyDetail.objects.get(user_id=user)
        except Exception as e:
            print(e)
        return context


class CompanyHomeView(TemplateView):
    model = CompanyDetail
    template_name = 'dashboard.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        user = self.request.user

        try:

            context['company_detail'] = CompanyDetail.objects.get(user_id=user)

        except Exception as e:
            print(e)
        return context


class CompanyProfileView(TemplateView):
    model = CompanyDetail
    template_name = 'company_profile.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        user = self.request.user

        context['cdetail'] = CompanyDetail.objects.get(user_id=user)
        try:

            context['company_detail'] = CompanyDetail.objects.get(user_id=user)

        except Exception as e:
            print(e)
        return context


class CompanyJobListView(ListView):
    template_name = 'company_joblist_todelete_update.html'
    model = JobPost

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        user = self.request.user
        print('company_joblist_ma kun user:', user)
        context['c_joblist'] = JobPost.objects.filter(company__user_id=user)
        try:

            context['company_detail'] = CompanyDetail.objects.get(user_id=user)

        except Exception as e:
            print(e)
        return context


class CompanyDetailView(CreateView):
    template_name = 'company_detail.html'
    model = CompanyDetail
    fields = ['company_name', 'province', 'district', 'company_address', 'company_type', 'contact_no', 'mobile_no',
              'company_image',
              'company_registration']

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        print('outise')
        if form.is_valid():
            print('inside')
            a = form.save(commit=False)

            a.user = request.user
            a.save()

            return redirect('company:jobpost')
        else:
            print(form.errors)

        return redirect('/custom/login')


class AppliedBySeekerView(CreateView):
    model = AppliedBySeeker
    fields = ['job_title', 'status', 'accepted']
    template_name = 'applied_job_seeker_list.html'

    def get(self, request, *args, **kwargs):

        user = request.user

        a = JobPost.objects.filter(company__user_id=user)
        n = CompanyDetail.objects.get(user_id=user)
        print('a ko value:', a)

        z = AppliedBySeeker.objects.all()

        context = {'jlist': a,
                   'sss': z,
                   'company_detail': n}

        return render(request, 'applied_job_seeker_list.html', context)

    def post(self, request, *args, **kwargs):
        form = self.get_form()

        if form.is_valid():
            a = form.save(commit=False)
            b = request.user.id

            jobsek = Jobseeker.objects.all()
            for i in jobsek:
                if i.user_id == b:
                    a.applicant_name_id = i.id

            a.save()

            return redirect('/')

        else:
            print({{form.errors}})

        return redirect('/custom/login')


class CompanyUpdateView(UpdateView):
    template_name = 'companydetail_update_form.html'
    model = CompanyDetail
    fields = ['company_name', 'province', 'district', 'company_address', 'company_type', 'contact_no', 'mobile_no',
              'company_image',
              'company_registration']
    success_url = reverse_lazy('company:cprofile')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        user = self.request.user

        context['cdetail'] = CompanyDetail.objects.get(user_id=user)
        context['company_detail'] = CompanyDetail.objects.get(user_id=user)
        return context


class JobUpdateView(UpdateView):
    template_name = 'jobpost_update_form.html'
    model = JobPost

    fields = ['job_title', 'category', 'level_of_job', 'vacancy', 'experience', 'education', 'salary', 'negotiable',
              'job_type', 'description', 'requirement',
              'deadline']

    success_url = reverse_lazy('company:cprofile')


class JobDeleteView(DeleteView):
    model = JobPost
    template_name = 'jobpost_confirm_delete.html'
    success_url = reverse_lazy('company:jobpost')

    # def get(self, request, *args, **kwargs):
    #     return self.delete(request, *args, **kwargs)


class ChangePassword(PasswordChangeView):
    template_name = 'password_change.html'
    success_url = reverse_lazy('home:home')


# class FaqModelForm(forms.ModelForm):
#
#     class Meta:
#         model = JobPost
#         fields = '__all__'
#


#
# class FAQCreateView(CreateView):
#     model = JobPost
#     form_class = FaqModelForm
#     template_name = 'create_faq.html'
#     success_url = '/faq'

def load_dis(request):
    pro = request.GET.get('province')
    districts = District.objects.filter(provience_id=pro)
    return render(request, 'dis_dropdown_list_options.html', {'districts': districts})


class CompanyNotificationView(CreateView):
    template_name = 'notify.html'
    model = AppliedBySeeker
    fields = ['job_title', 'status', 'accepted']

    def get_context_data(self, *, object_list=None, **kwargs):
        user = self.request.user

        a = JobPost.objects.filter(company__user_id=user)
        z = AppliedBySeeker.objects.all()

        context = super().get_context_data(**kwargs)
        context['jlist'] = a
        context['sss'] = z

        try:

            context['company_detail'] = CompanyDetail.objects.get(user_id=user)

        except Exception as e:
            print(e)
        return context

    def post(self, request, *args, **kwargs):
        # form = self.get_form()
        print("outside form")

        b = request.user.id
        print(b)

        jobsek = AppliedBySeeker.objects.all()
        for i in jobsek:
            print("inside")
            print(i.job_title.company.user.id)
            a = AppliedBySeeker.objects.filter(job_title__company__user_id=b)
            getting = int(request.POST.get("hiddenid"))

            print("getting", getting)
            for j in a:
                z = j.job_title_id
                print("j ko ",z)
                if getting == z:
                    print("z ko value", z)
                    j.status = True
                    j.save()

            return redirect('company:companynotify')

        return redirect('/custom/login')
