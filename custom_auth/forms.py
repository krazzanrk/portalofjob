from django import forms
from custom_auth.models import User


class UserRegistrationForm(forms.ModelForm):
    class Meta:
        model = User

        fields = {
            'username',
            'email',
            'password',
            'role'
        }

    def save(self, commit=True):
        user = super(UserRegistrationForm, self).save(commit=False)
        user.set_password(self.cleaned_data['password'])
        if commit:
            user.save()
        return user
