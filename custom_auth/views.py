from django.contrib.auth.views import LoginView, LogoutView, PasswordChangeView, PasswordResetView, \
    PasswordResetDoneView, PasswordResetConfirmView
from django.http import request
from django.urls import reverse, reverse_lazy
from django.views.generic import CreateView, TemplateView
from django.contrib.sites.shortcuts import get_current_site
from django.shortcuts import render, redirect
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.template.loader import render_to_string
from custom_auth.forms import *


# Create your views here.
from custom_auth.token import account_activation_token


class RegistrationView(CreateView):
    model = User
    # template_name = ''
    form_class = UserRegistrationForm
    success_url = '/custom/login'
    template_name = 'registration/register.html'

    def post(self, request, *args, **kwargs):
        form=UserRegistrationForm(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            user.is_active = False
            user.save()
            current_site = get_current_site(request)
            subject = 'Activate Your MySite Account'
            message = render_to_string('registration/account_activation_email.html', {
                'user': user,
                'domain': current_site.domain,
                'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                'token': account_activation_token.make_token(user),
            })
            user.email_user(subject, message)
            return redirect('custom_auth:activation_complete')

        else:
            form=UserRegistrationForm

            return render(request, 'registration/register.html', {'form': form})



class CustomLoginView(LoginView):
    template_name = 'registration/login.html'

    def get_success_url(self):
        user = self.request.user

        if user.role == 'company':
            return reverse('company:jobhome')

        else:
            return reverse('jobseeker:sindex')


class CustomLogoutView(LogoutView):
    template_name = 'index.html'

    def get(self, request, *args, **kwargs):
        return redirect('home:home')


class UserPasswordResetView(PasswordResetView):
    template_name = 'registration/password_reset_form1.html'
    success_url = reverse_lazy('custom_auth:password_reset_done')
    subject_template_name = 'registration/password_send_subject.txt'
    email_template_name = 'registration/password_reset_emailsend.html'


class UserPasswordResetDoneView(PasswordResetDoneView):
    template_name = 'registration/password_reset_doneok.html'


class UserPasswordResetConfirmView(PasswordResetConfirmView):
    template_name = 'registration/password_reset_confirmok.html'
    success_url = reverse_lazy('home:home')
    form_valid_message = "Your password was changed!"



def activate(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except (TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None

    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.profile.email_confirmed = True
        user.save()

        return redirect('custom_auth:login')


class ActivationDoneView(TemplateView):
    template_name = 'registration/account_activation_done.html'


