from django.urls import path

from custom_auth.views import *

app_name = 'custom_auth'

urlpatterns = [
    path('login/', CustomLoginView.as_view(), name='login'),
    path('register/', RegistrationView.as_view(), name='register'),

    path('logout/', CustomLogoutView.as_view(), name='logout'),
    path('password_reset', UserPasswordResetView.as_view(), name='password_reset'),
    path('password_reset/done', UserPasswordResetDoneView.as_view(), name='password_reset_done'),
    path('reset/<uidb64>/<token>', UserPasswordResetConfirmView.as_view(), name='password_reset_confirmok'),
    path('activate/<uidb64>/<token>',activate, name='activate'),
    path('activationdone/',ActivationDoneView.as_view(),name='activation_complete')

]
