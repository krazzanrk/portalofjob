from django.urls import path
from .views import *

app_name = 'cms'

urlpatterns = [

    path('', AboutUsView.as_view(), name='about'),
    path('contact/', ContactUsView.as_view(), name='contact'),

]
